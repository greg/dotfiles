#!/usr/bin/env bash
set -e

# Define the target directory
TARGET_DIR="$HOME"

# Clone the dotfiles repository
git clone --depth=1 https://gitlab.com/greg/dotfiles.git "$TARGET_DIR/dotfiles"

# Move the dotfiles to the home directory, backing up existing files
for file in .tmux.conf .gitignore .vimrc .bashrc; do
  if [[ -e "$TARGET_DIR/$file" ]]; then
    mv -i "$TARGET_DIR/$file" "$TARGET_DIR/$file.bak"
  fi
  mv -i "$TARGET_DIR/dotfiles/$file" "$TARGET_DIR/"
done

# Remove the dotfiles directory
rm -rf "$TARGET_DIR/dotfiles"

echo "You're all set, enjoy!"
